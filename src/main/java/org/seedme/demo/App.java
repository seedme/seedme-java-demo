package org.seedme.demo;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;
import org.json.JSONArray;
import org.json.JSONObject;

import org.seedme.client.FileEntry;
import org.seedme.client.RequestException;
import org.seedme.client.SeedMe;

/**
 * You should add "-ea" to VM arguments when running to make assertions work!
 *
 */

public class App {
	public static void main(String[] args) throws NumberFormatException, ParseException, RequestException, ClientProtocolException, IOException
	{
		String sample_dir = "/Users/dimm/code/seedme_client";
		String download_to = "/Users/dimm/tmp/";
		
		//Create new SeedMe Object
		SeedMe sm = new SeedMe();
		
		//Set Auth file
		sm.set_auth_via_file(sample_dir+"/seedme_prod.txt");

		//Create a HashMap of Keyvalues
		Map<String, String> keyvalues = new HashMap<String, String>();
		keyvalues.put("pressure", "10kpa");
		keyvalues.put("temperature", "295k");
		
		//Create tags and add to ArrayList
		ArrayList<String> tags = new ArrayList<String>();
		tags.add("tag1");
		tags.add("tag2");
		
		//Create tickers in ArrayList
		ArrayList<String> tickers = new ArrayList<String>();
		tickers.add("ticker1");
		tickers.add("ticker2");

		//Create HashSet of files to upload
		FileEntry fEntry = null;
		HashSet<FileEntry> files = new HashSet<FileEntry>();	
		fEntry = new FileEntry(sample_dir+"/sample/files/doc.pdf");
		//Set the title, description, etc before adding to HashSet
		fEntry.setTitle("TestTitle");
		files.add(fEntry);
		
		//Reset the file entry, if adding more than one. //Second chunk, carry on title, and 
		fEntry = null;
		fEntry = new FileEntry(sample_dir+"/sample/files/how_it_works.pdf");
		fEntry.setDescription("TestDescription");
		files.add(fEntry);
		
		fEntry = null;
		fEntry = new FileEntry(sample_dir+"/sample/files/progress_bars.ipynb");
		files.add(fEntry);
		
		int col_id = Integer.parseInt(sm.create_collection( "public", "test1@seedme.org test2@seedme.org" , false, "Test_collection", "Test description" , "XYS" , "Apache", false, null, null, null, files,   null, null));
		System.out.println("Created collection "+col_id);
		
		
		////////////////////////////////////////////////////////////////////////////////////////////
		// CHECK RESULTS
		////////////////////////////////////////////////////////////////////////////////////////////
		
		JSONObject json = new JSONObject(Request.Get(sm.getServicesUrl()+"/"+col_id+"?list=all").
		    	addHeader("username", sm.getUsername()).
		    	addHeader("apikey", sm.getApikey()).
		    	execute().returnContent().asString());
//		System.out.println(json.toString());
		assert(json.getString("collection_id").equals(col_id+""));
		assert(json.getString("title").equals("Test_collection"));
		assert(json.getString("privacy").equals("public"));
		assert(Arrays.equals(json.getString("sharing").split(" +"), new String[] {"test1@seedme.org","test2@seedme.org"})) : "value is "+json.getString("sharing");
		assert(json.getString("description").equals("Test description"));
		assert(json.getString("credits").equals("XYS"));
		
		HashSet<String> files_added = new HashSet<String>();
		files_added.add("how_it_works.pdf");
		files_added.add("progress_bars.ipynb");
		files_added.add("TestTitle"); // doc.pdf
		JSONArray json_files = json.getJSONArray("files");
		for(int i=0; i<json_files.length(); i++) {
			files_added.remove(json_files.getJSONObject(i).getString("title"));
		}
		assert(files_added.size() == 0): "files not found are "+files_added;

		////////////////////////////////////////////////////////////////////////////////////////////
		// END CHECK RESULTS
		////////////////////////////////////////////////////////////////////////////////////////////
		

		//Create sequences
		LinkedList<String> filepaths = new LinkedList<String>();
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0044.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0045.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0046.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0047.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0048.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0049.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0050.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0051.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0052.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0053.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0054.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0055.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0056.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0057.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0058.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0059.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0060.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0061.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0062.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0063.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0064.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0065.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0066.jpg");
		filepaths.add(sample_dir+"/sample/sequences/mandelbrot/amr0067.jpg");


		FileEntry fEntry2 = new FileEntry(filepaths);
		fEntry2.setTitle("Test");
		fEntry2.setDescription("desc");
		fEntry2.setEncode(true);
		HashSet<FileEntry> sequence = new HashSet<FileEntry>();
		sequence.add(fEntry2);

		files = new HashSet<FileEntry>();
		FileEntry video = new FileEntry(sample_dir+"/sample/videos/air.mp4");
		files.add(video);

		FileEntry plot = new FileEntry(sample_dir+"/sample/plots/composite.png");
		files.add(plot);

		sm.update_collection(col_id, "private", "test3@seedme.org", false, "Test title modified", "Test description modified", "modified", null, false, null, null, null, null, files, null);
		System.out.println("Updated collection "+col_id);

		////////////////////////////////////////////////////////////////////////////////////////////
		// CHECK RESULTS
		////////////////////////////////////////////////////////////////////////////////////////////
		
		files_added.add("how_it_works.pdf");
		files_added.add("progress_bars.ipynb");
		files_added.add("TestTitle"); // doc.pdf
		files_added.add("air.mp4");
		files_added.add("composite.png");
		files_added.add("Test");
		
		json = new JSONObject(Request.Get(sm.getServicesUrl()+"/"+col_id+"?list=all").
		    	addHeader("username", sm.getUsername()).
		    	addHeader("apikey", sm.getApikey()).
		    	execute().returnContent().asString());
		assert(json.getString("collection_id").equals(col_id+"")) : "value is "+json.getString("collection_id");
		assert(json.getString("title").equals("Test title modified")) : "value is "+json.getString("title");
		assert(json.getString("description").equals("Test description modified")) : "value is "+json.getString("description");
		assert(json.getString("privacy").equals("private")) : "value is "+json.getString("privacy");
		assert(Arrays.equals(json.getString("sharing").split(" +"), new String[] {"test1@seedme.org","test2@seedme.org", "test3@seedme.org"})) : "value is "+json.getString("sharing");
		assert(json.getString("credits").equals("modified")) : "value is "+json.getString("credits");
		
		json_files = json.getJSONArray("files");
		for(int i=0; i<json_files.length(); i++) {
			files_added.remove(json_files.getJSONObject(i).getString("title"));
			if(json_files.getJSONObject(i).getString("title").equals("Test")){
				JSONArray seq_files = json_files.getJSONObject(i).getJSONArray("files");
				for(int j=44; j<=67; j++)
					assert(((JSONObject)seq_files.remove(0)).get("title").equals("amr00"+j+".jpg"));
			}
		}
		assert(files_added.size() == 0): "files not found are "+files_added;

		////////////////////////////////////////////////////////////////////////////////////////////
		// END CHECK RESULTS
		////////////////////////////////////////////////////////////////////////////////////////////
		
		
		Map<String, String> keyval_hash = new HashMap<String, String>();
		keyval_hash.put("Density", "33.33");
		sm.add_keyvalue(col_id, keyval_hash);

		List<String> my_tickers = new ArrayList<String>();
		my_tickers.add("step 10: convergence 0%");
		my_tickers.add("step 30: convergence 25%");
		my_tickers.add("step 50: convergence 39%");
		my_tickers.add("step 70: convergence 50%");
		sm.add_ticker(col_id, my_tickers);
		
		List<String> my_tags = new ArrayList<String>();
		my_tags.add("tag1");
		my_tags.add("tag2");
		sm.add_tag(col_id,  my_tags);	
		
		//Query for URLs
		try {
			ArrayList<JSONObject> s = sm.query( col_id+"", null, "url",  null );
			System.out.println("Collection url:"+s);
		} catch (RequestException e) {
			e.printStackTrace();
		}	
		
		//Query and Download only mp4s
		try {
			sm.download(col_id+"", "all", download_to, 0, 0, true);
			System.out.println("Downloaded files to "+download_to);
		} catch (RequestException e) {
			e.printStackTrace();
		}	

		////////////////////////////////////////////////////////////////////////////////////////////
		// CHECK RESULTS
		////////////////////////////////////////////////////////////////////////////////////////////
		
		json = new JSONObject(Request.Get(sm.getServicesUrl()+"/"+col_id+"?list=all").
		    	addHeader("username", sm.getUsername()).
		    	addHeader("apikey", sm.getApikey()).
		    	execute().returnContent().asString());
//		System.out.println(json.toString());
		assert(json.getJSONObject("keyvalues").getString("Density").equals("33.33")) : "value is "+json.getJSONObject("keyvalues").getString("Density");
		assert(json.getJSONArray("tags").get(0).toString().equals("tag1")) : "value is "+json.getJSONArray("tags").get(0).toString();
		assert(json.getJSONArray("tags").get(1).toString().equals("tag2")) : "value is "+json.getJSONArray("tags").get(1).toString();
		assert(((JSONObject)(json.getJSONArray("tickers").get(0))).getString("ticker").equals("step 10: convergence 0%")) : "value is "+json.getJSONArray("tickers");
		assert(((JSONObject)(json.getJSONArray("tickers").get(1))).getString("ticker").equals("step 30: convergence 25%")) : "value is "+json.getJSONArray("tickers");
		assert(((JSONObject)(json.getJSONArray("tickers").get(2))).getString("ticker").equals("step 50: convergence 39%")) : "value is "+json.getJSONArray("tickers");
		assert(((JSONObject)(json.getJSONArray("tickers").get(3))).getString("ticker").equals("step 70: convergence 50%")) : "value is "+json.getJSONArray("tickers");
		
		////////////////////////////////////////////////////////////////////////////////////////////
		// END CHECK RESULTS
		////////////////////////////////////////////////////////////////////////////////////////////
		
//		sm.update_transfer(col_id, "seedme@sdsc.edu");
		
		if(new File(sample_dir+"/seedme_demo.txt").exists()) {
			sm.set_auth_via_file(sample_dir+"/seedme_demo.txt");
			json = new JSONObject(Request.Get(sm.getServicesUrl()+"/"+col_id+"?list=all").
			    	addHeader("username", sm.getUsername()).
			    	addHeader("apikey", sm.getApikey()).
			    	execute().returnContent().asString());
			assert(json.getString("description").equals("Test description modified")) : "value is "+json.getString("description");
	//		System.out.println(json.toString());
		} else {
			System.out.println("Please download demo account credentials to "+sample_dir+"/seedme_demo.txt to test collection ownership transfer.");
		}
		
//		sm.delete_collection(col_id);
		
	}
	
}
